K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
ETYPE="sources"
inherit kernel-2 eutils
detect_version

DESCRIPTION="vanilla plus patches"
HOMEPAGE=""
SRC_URI="${KERNEL_URI}"

LICENSE=""
SLOT="0"
KEYWORDS="ppc ppc64"
IUSE=""

DEPEND=""
RDEPEND=""

UNIPATCH_STRICT=1
UNIPATCH_LIST="${FILESDIR}/sq24.patch.gz"




pkg_postinst() {
	postinst_sources
}
