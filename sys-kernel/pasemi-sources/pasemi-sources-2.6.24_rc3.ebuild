
#K_WANT_GENPATCHES="base extras"
#K_GENPATCHES_VER="15"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
ETYPE="sources"
inherit kernel-2 eutils
detect_version

DESCRIPTION="vanilla plus ps3 patches"
HOMEPAGE=""

#PS3_PATCHES_URI="http://dev.gentoo.org/~ranger/downloads/ps3/${PV}-ps3updates.diff.bz2"

SRC_URI="${KERNEL_URI}"

KEYWORDS="ppc ppc64"


src_unpack() {
	kernel-2_src_unpack
	#cd ${S}
	cd ..
	pwd
	epatch ${FILESDIR}/sq24.patch.gz
	epatch ${FILESDIR}/eth_fix.patch.gz
	epatch ${FILESDIR}/i2c.patch.gz
	epatch ${FILESDIR}/rtc.patch.gz
	epatch ${FILESDIR}/rtc_class.patch.gz
	epatch ${FILESDIR}/freeskb.patch.gz
}


pkg_postinst() {
	postinst_sources
}

