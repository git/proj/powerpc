K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
ETYPE="sources"
inherit kernel-2 eutils
detect_version

DESCRIPTION="vanilla plus patches"
HOMEPAGE=""
SRC_URI="${KERNEL_URI}"

LICENSE=""
SLOT="0"
KEYWORDS="ppc ppc64"
IUSE=""

DEPEND=""
RDEPEND=""

UNIPATCH_STRICT=1
UNIPATCH_LIST="${FILESDIR}/sq24.patch.gz ${FILESDIR}/i2c.patch.gz ${FILESDIR}/rtc.patch.gz ${FILESDIR}/rtc_class.patch.gz"




pkg_postinst() {
	postinst_sources
}
