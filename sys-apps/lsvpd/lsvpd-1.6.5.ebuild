# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-libs/librtas/librtas-1.3.3.ebuild,v 1.2 2008/04/21 19:54:50 ranger Exp $

inherit eutils

DESCRIPTION="Linux Diagnostics Tools "
SRC_URI="mirror://sourceforge/linux-diag/${P}.tar.gz"
HOMEPAGE="http://linux-diag.sourceforge.net/Lsvpd.html/"

SLOT="0"
LICENSE="IPL-1"
KEYWORDS="~ppc ~ppc64"
IUSE=""

DEPEND="sys-libs/libvpd
		sys-libs/librtas
		sys-apps/sg3_utils"

src_unpack() {
	unpack ${A}
}

src_install() {
	make DESTDIR="${D}" install || die "Compilation failed"
	dodoc README COPYRIGHT

}
